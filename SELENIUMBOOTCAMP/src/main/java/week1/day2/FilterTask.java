package week1.day2;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FilterTask {

	public static void main(String[] args) throws InterruptedException {
		// Login to https://login.salesforce.com
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("-disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		Thread.sleep(5000);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		//Login to https://login.salesforce.com
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(7000);
		//	 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		//	 3. Click View All and click Sales from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		//	 4. Click on the Dropdown icon in the Opportunities tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Content");
		driver.findElement(By.xpath("//mark[text()='Content']")).click();
		//5. Click View All from Today's Task
		driver.findElement(By.xpath("(//a[@class='viewAllLink']/span[text()='View All'])[2]")).click();
		Thread.sleep(3000);
		WebElement splitViewDropDown = driver.findElement(By.xpath("//span[text()='Display as Split View']"));
		JavascriptExecutor jsesplitview = (JavascriptExecutor)driver;
		jsesplitview.executeScript("arguments[0].click();", splitViewDropDown);
		
		WebElement tableView = driver.findElement(By.xpath("//span[text()='Table']"));
		JavascriptExecutor jseTabView = (JavascriptExecutor)driver;
		jseTabView.executeScript("arguments[0].click();", tableView); 
		
		driver.findElement(By.xpath("//button[@title='Select a List View']")).click();
		driver.findElement(By.xpath("//span[@class=' virtualAutocompleteOptionText' and text()='Open Tasks']")).click();
		driver.findElement(By.xpath("//button[@title='Show filters']")).click();
	
		driver.findElement(By.xpath("//div[@class='filterLabel' and text()='Filter by Owner']")).click();
		//driver.findElement(By.xpath("//span[text()='My tasks']")).click();
		driver.findElement(By.xpath("(//span[text()='All tasks'])[2]")).click();
		
		
		driver.findElement(By.xpath("//button[@class='slds-button slds-button--neutral doneButton uiButton']/span[text()='Done']")).click();
		driver.findElement(By.linkText("Add Filter")).click();
		
		driver.findElement(By.xpath("//button[@data-value='Assigned Alias']")).click();
			
		//WebElement fieldLabel = driver.findElement(By.xpath("//span[@title='equals']"));
		//JavascriptExecutor jsefieldLabel = (JavascriptExecutor)driver;
		//jsefieldLabel.executeScript("arguments[0].click();", fieldLabel); 
		
		WebElement fieldStatus = driver.findElement(By.xpath("//label[text()='Field']//following::span[text()='Status']"));
		JavascriptExecutor jsefieldStatus = (JavascriptExecutor)driver;
		jsefieldStatus.executeScript("arguments[0].click();", fieldStatus); 
		
		driver.findElement(By.xpath("//label[text()='Operator']//following::button[contains(@class,'slds-combobox__input')]")).click();
		
		WebElement operator = driver.findElement(By.xpath("//span[@title='equals']"));
		JavascriptExecutor jseOperator = (JavascriptExecutor)driver;
		jseOperator.executeScript("arguments[0].click();", operator); 
		
		//driver.findElement(By.xpath("//label[@class='inputLabel uiLabel']//following::input[contains(@class,'filterTextInput ')]")).click();
		//WebElement value = driver.findElement(By.xpath("//input[contains(@class,'filterTextInput ')]"));
		//JavascriptExecutor jsevalue = (JavascriptExecutor)driver;
		//jsevalue.executeScript("arguments[0].click();", value); 
		
		//WebElement valueFieldClick = driver.findElement(By.xpath("//div[@class='forceRecordLayout']//input[1]"));
		//JavascriptExecutor jsevalueFieldClick = (JavascriptExecutor)driver;
		//jsevalueFieldClick.executeScript("arguments[0].click();", valueFieldClick); 
		
		driver.findElement(By.xpath("//div[@class='forceRecordLayout']//input[1]")).click();
		
		
		WebElement valueSelection = driver.findElement(By.xpath("//a[@title='In Progress']"));
		JavascriptExecutor jsevalueSelection = (JavascriptExecutor)driver;
		jsevalueSelection.executeScript("arguments[0].click();", valueSelection); 
		
		WebElement doneButton = driver.findElement(By.xpath("//span[text()='Done']"));
		JavascriptExecutor jsedoneButton = (JavascriptExecutor)driver;
		jsedoneButton.executeScript("arguments[0].click();", doneButton);
		

		driver.findElement(By.xpath("//button[@class='slds-button slds-button_brand saveButton headerButton']")).click();
	}

}
