package normalScripts;




import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.it.Date;
import io.github.bonigarcia.wdm.WebDriverManager;

public class EditOpportunity {
//@Test
	//public void createOpportunity() throws InterruptedException {
	public static void main(String[] args) throws InterruptedException {	
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("-disable-notifications");
		WebDriver driver = new ChromeDriver(options);
		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		//Thread.sleep(5000);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		//Login to https://login.salesforce.com
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(5000);
		String title = driver.getTitle();
		System.out.println("Title :" + title);
		
		WebDriverWait toggleButtonwait = new WebDriverWait(driver,30);
		WebElement toggleButton = driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"));
		toggleButtonwait.until(ExpectedConditions.visibilityOf(toggleButton));
				
		//	 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		//	 3. Click View All and click Opportunities from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		//	 4. Click on the Dropdown icon in the Opportunities tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Opportunities");
		driver.findElement(By.xpath("//mark[text()='Opportunities']")).click();
		
		WebDriverWait searchListwait = new WebDriverWait(driver,30);
		WebElement searchList = driver.findElement(By.xpath("//input[@placeholder='Search this list...']"));
		searchListwait.until(ExpectedConditions.visibilityOf(searchList));
				
				
//		 5. Click on New Individual
			driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys("Sales Force Automation By Kavitha T A");
			Thread.sleep(5000);
			//	 6. Enter the Last Date as 'Kumar'
			//String lastName = "Kavitha T A";
			WebElement showActions = driver.findElement(By.xpath("//table[@aria-label='Recently Viewed']/tbody/tr[1]/td//span[text()='Show Actions']"));
			
			 JavascriptExecutor jseshowActions = (JavascriptExecutor)driver;
			 jseshowActions.executeScript("arguments[0].click();", showActions);
			 
			
			 WebElement selectEdit = driver.findElement(By.xpath("//a[@title='Edit']"));
			 JavascriptExecutor jseselectEdit =(JavascriptExecutor)driver;
			 jseselectEdit.executeScript("arguments[0].click();", selectEdit);
			
			 driver.findElement(By.xpath("//label[text()='Stage']//following::button[@aria-label='Stage, Needs Analysis']")).click();
			 
			 WebElement stageDropDown = driver.findElement(By.xpath("//span[@title='Perception Analysis']"));
			 JavascriptExecutor jsetitle = (JavascriptExecutor) driver;
			 jsetitle.executeScript("arguments[0].click();",stageDropDown);
			 
			 
			 driver.findElement(By.xpath("//label[text()='Delivery/Installation Status']//following::button[@aria-label='Delivery/Installation Status, --None--']")).click();
			 
			 WebElement installationStatus = driver.findElement(By.xpath("//span[@title='In progress']"));
			 JavascriptExecutor jseStatus = (JavascriptExecutor) driver;
			 jseStatus.executeScript("arguments[0].click();",installationStatus);
			 
			 driver.findElement(By.xpath("//label[text()='Description']//following::textarea[@class='slds-textarea']")).sendKeys("SalesForce");
			 
				
				/*
				 * WebDriverWait clicksavebutton = new WebDriverWait(driver,30); WebElement
				 * saveButton = driver.findElement(By.xpath("//button[text()='Save']"));
				 * clicksavebutton.until(ExpectedConditions.visibilityOf(saveButton));
				 */
				 
				
			 WebElement save_Button = driver.findElement(By.xpath("//button[text()='Save']"));
			 JavascriptExecutor jseSaveButton = (JavascriptExecutor) driver;
			 jseSaveButton.executeScript("arguments[0].click();",save_Button);
			 
			 String editMessage = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
			 System.out.println("EditMessage : "+editMessage);
			 
			  String stage = driver.findElement(By.xpath("//table[@aria-label='Recently Viewed']/tbody/tr[1]/td[5]")).getText();
			  System.out.println("Stage :" + stage);
			  if (stage.equals("Perception Analysis"))
				  System.out.println("Opportunity is Edited Successfully");
			  else
				  System.out.println("Opportunity is Not Edited");
			 
			 
			/*
			 * driver.findElement(By.xpath(
			 * "(//table[@class='slds-datepicker__month']/tbody/tr/td)[5]")).click(); //
			 * 7.Click save and verify Opportunity Name driver.findElement(By.
			 * xpath("//label[text()='Opportunity Name']//following::input[@name='Name']")).
			 * sendKeys("Test");
			 * 
			 * WebElement stage = driver.findElement(By.xpath(
			 * "//button[contains(@class,'slds-combobox__input')]")); JavascriptExecutor jse
			 * = (JavascriptExecutor)driver; jse.executeScript("arguments[0].click();",
			 * stage);
			 * 
			 * WebElement stageOption =
			 * driver.findElement(By.xpath("//span[@title='Needs Analysis']"));
			 * JavascriptExecutor executor3 = (JavascriptExecutor)driver;
			 * executor3.executeScript("arguments[0].click();", stageOption);
			 * 
			 * Thread.sleep(5000); // driver.findElement(By.
			 * xpath("//button[contains(@class,'slds-combobox__input')]/span[text()='Needs Analysis']"
			 * )).click();
			 * driver.findElement(By.xpath("//button[@name='SaveEdit']")).click(); //Verify
			 * the Alert message (Complete this field) displayed for Name and Stage
			 * Thread.sleep(5000); String s = driver.findElement(By.
			 * xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"
			 * )).getText(); System.out.println(s);
			 * if(s.equals("Opportunity \"Test\" was created."))
			 * System.out.println("Opportunity is created successfully"); else
			 * System.out.println("Opportunity is not created");
			 */	}

}
