package week1.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static String[][] readData(String sheetName, String fileName) throws IOException {
		//XSSFWorkbook wb = new XSSFWorkbook("./TestData/CreateLead.xlsx");
		XSSFWorkbook wb = new XSSFWorkbook("./TestData/"+fileName+".xlsx");
		XSSFSheet ws = wb.getSheet(sheetName);
		
		//To get the rows excluding Header row
		int lastRowNum = ws.getLastRowNum();
		System.out.println("lastRowNum:" +lastRowNum);
		
		//To get the rows including Header row
		//int physicalNumberOfRows = ws.getPhysicalNumberOfRows();
		//System.out.println("physicalNumberOfRows:" +physicalNumberOfRows); 
		
		//To get the number of columns
		short lastCellNum = ws.getRow(0).getLastCellNum();
		System.out.println("lastCellNum:" +lastCellNum);
		
		//Declare 2D array with rowcount and cellcount
		String[][] data = new String[lastRowNum][lastCellNum];
		
		for (int i=1;i<=lastRowNum;i++) {
			for(int j=0;j<lastCellNum;j++) {
			XSSFRow row = ws.getRow(i);
		    XSSFCell cell = row.getCell(j);
		
		String stringCellValue = cell.getStringCellValue();
		
		data[i-1][j] = stringCellValue;
		System.out.println(stringCellValue);
		
		}
		}
		
		wb.close();
		return data;
	}

}
