package week1.day2;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class OpportunitiessortbyClosedate {

	public static void main(String[] args) throws InterruptedException {
		// Login to https://login.salesforce.com
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("-disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		Thread.sleep(5000);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(7000);
		//	 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		//	 3. Click View All and click Sales from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		//	 4. Click on the Dropdown icon in the Opportunities tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Sales");
		driver.findElement(By.xpath("(//mark[text()='Sales'])[3]")).click();
		 //Click on Opportunity tab
		
		//driver.findElement(By.xpath("//a[@title='Opportunities']/span")).click();
		WebElement opportunities = driver.findElement(By.xpath("//a[@title='Opportunities']/span"));
        JavascriptExecutor jseOpp = (JavascriptExecutor)driver;
        jseOpp.executeScript("arguments[0].click();", opportunities); 
		//5. Select the Table view
        Thread.sleep(5000);
        //driver.findElement(By.xpath("//span[text()='Display as Table']")).click();
        WebElement tableView = driver.findElement(By.xpath("//span[text()='Display as Table']"));
        JavascriptExecutor jseTabView = (JavascriptExecutor)driver;
        jseTabView.executeScript("arguments[0].click();", tableView); 
        
      //6. Sort the Opportunities by Close Date in ascending order
        driver.findElement(By.xpath("//span[text()='Table']")).click();
        WebElement closeDateSort = driver.findElement(By.xpath("//a[@class='toggle slds-th__action slds-text-link--reset ']/span[text()='Close Date']"));
        JavascriptExecutor jseDateSort = (JavascriptExecutor)driver;
        jseDateSort.executeScript("arguments[0].click();", closeDateSort); 
               
        Thread.sleep(5000);
        
		//7. Verify the Opportunities displayed in ascending order by Close date
        //List<Date> values = new ArrayList<Date>();
        List<WebElement> rows = driver.findElements(By.xpath("//table[contains(@class,'uiVirtualDataTable')]/tbody/tr"));
      driver.findElement(By.xpath("//div[contains(@class,'uiScroller scroller-wrapper')]")).sendKeys(Keys.CONTROL, Keys.END);
      // JavascriptExecutor js1 = (JavascriptExecutor) driver;
      // js1.executeScript("window.scrollTo(0, document.body.scrollHeight)");
       //Actions builder = new Actions(driver);
      // builder.sendKeys(Keys.PAGE_DOWN).build().perform();
     
        int rowCount = rows.size();
        System.out.println(rowCount);
        for(int i=1; i<=rowCount;i++) {
        	 //System.out.println(i);
			String text = driver.findElement(By.xpath("//table[contains(@class,'uiVirtualDataTable')]/tbody/tr["+i+"]/td[6]//span[@class='uiOutputDate']")).getText();
			System.out.println(text);
		}
        System.out.println(rowCount);
        //for(int i =1; i<=)
//		 
	}

}
