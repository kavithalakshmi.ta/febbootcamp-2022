package week1.day2;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SalesForce_Classic_NewEvent extends BaseClass{
@Test (dataProvider = "fetchData")
	public  void salesForce_Classic_NewEvent(String stringCellValue) throws InterruptedException, IOException {
		//Thread.sleep(7000);
		driver.findElement(By.className("switch-to-lightning")).click();
		System.out.println("Value Is:" +stringCellValue);
		Thread.sleep(7000);
		
		//	 2. Click on View Profile icon
		driver.findElement(By.xpath("//span[@class='userProfileCardTriggerRoot oneUserProfileCardTrigger']")).click();
				
		//3. Click on Switch to Salesforce Classic
		driver.findElement(By.linkText("Switch to Salesforce Classic")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[text()='New Event']")).click();
		driver.findElement(By.xpath("(//input[@data-fieldname='subject'])[1]")).click();
		
		driver.findElement(By.xpath("(//input[@data-fieldname='subject'])[1]")).sendKeys(stringCellValue);
		driver.findElement(By.xpath("//input[@data-fieldname='startdatetime']")).click();
		driver.findElement(By.xpath("//table[@class='calDays']/tbody/tr[5]/td[text()='25']")).click();
		
		driver.findElement(By.xpath("//input[@data-fieldname='enddatetime']")).click();
		driver.findElement(By.xpath("//table[@class='calDays']/tbody/tr[5]/td[text()='26']")).click();
}


@DataProvider(name= "fetchData")
public String[][] dataFromExcel() throws IOException {
	//String[][] readData = ReadExcel.readData();
	//return readData;
	return ReadExcel.readData("Sheet1", "ReadExcel");
			
}			
	
}

