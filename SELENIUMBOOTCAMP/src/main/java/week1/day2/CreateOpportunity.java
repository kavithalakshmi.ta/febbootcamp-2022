package week1.day2;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateOpportunity extends BaseClass{
@Test
	public void createOpportunity() throws InterruptedException {
		
		//	 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		//	 3. Click View All and click Opportunities from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		//	 4. Click on the Dropdown icon in the Opportunities tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Opportunities");
		driver.findElement(By.xpath("//mark[text()='Opportunities']")).click();
//		 5. Click on New Individual
			driver.findElement(By.xpath("//a[@title='New']/div")).click();
			Thread.sleep(5000);
			//	 6. Enter the Last Date as 'Kumar'
			//String lastName = "Kavitha T A";
			driver.findElement(By.xpath("//input[@name='CloseDate']")).click();
			driver.findElement(By.xpath("(//table[@class='slds-datepicker__month']/tbody/tr/td)[5]")).click();
			//	 7.Click save and verify Opportunity Name
			driver.findElement(By.xpath("//label[text()='Opportunity Name']//following::input[@name='Name']")).sendKeys("Test");
			
	        WebElement stage = driver.findElement(By.xpath("//button[contains(@class,'slds-combobox__input')]"));
	        JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", stage); 
			
			 WebElement stageOption = driver.findElement(By.xpath("//span[@title='Needs Analysis']"));
		        JavascriptExecutor executor3 = (JavascriptExecutor)driver;
		        executor3.executeScript("arguments[0].click();", stageOption);

			Thread.sleep(5000);
		//	driver.findElement(By.xpath("//button[contains(@class,'slds-combobox__input')]/span[text()='Needs Analysis']")).click();
			driver.findElement(By.xpath("//button[@name='SaveEdit']")).click();
			//Verify the Alert message (Complete this field) displayed for Name and Stage
			Thread.sleep(5000);
			String s = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
			System.out.println(s);
			if(s.equals("Opportunity \"Test\" was created."))
				System.out.println("Opportunity is created successfully");
			else
				System.out.println("Opportunity is not created");
	}

}
