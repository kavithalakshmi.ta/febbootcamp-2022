package week1.day2;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditNewTask {

	public static void main(String[] args) throws InterruptedException {
		// Login to https://login.salesforce.com
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("-disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		Thread.sleep(5000);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		//Login to https://login.salesforce.com
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(7000);
		//	 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		//	 3. Click View All and click Sales from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		//	 4. Click on the Dropdown icon in the Opportunities tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Content");
		driver.findElement(By.xpath("//mark[text()='Content']")).click();
		//5. Click View All from Today's Task
		driver.findElement(By.xpath("(//a[@class='viewAllLink']/span[text()='View All'])[2]")).click();
		Thread.sleep(3000);
		WebElement splitViewDropDown = driver.findElement(By.xpath("//span[text()='Display as Split View']"));
		JavascriptExecutor jsesplitview = (JavascriptExecutor)driver;
		jsesplitview.executeScript("arguments[0].click();", splitViewDropDown);
		
		WebElement tableView = driver.findElement(By.xpath("//span[text()='Table']"));
		JavascriptExecutor jseTabView = (JavascriptExecutor)driver;
		jseTabView.executeScript("arguments[0].click();", tableView); 
		
		//6. Click on Show one more Action and click New Task
		driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys("Email");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//table[contains(@class,'uiVirtualDataTable')]/tbody/tr[1]/td//label[@class='slds-checkbox']")).click();
		Thread.sleep(3000);
		WebElement searchElement = driver.findElement(By.xpath("//table[contains(@class,'uiVirtualDataTable')]/tbody/tr[1]/td[7]//a"));
		JavascriptExecutor jsesearchElement = (JavascriptExecutor)driver;
		jsesearchElement.executeScript("arguments[0].click();", searchElement); 
		
		WebElement clickEdit = driver.findElement(By.xpath("//a[@title='Edit']"));
		JavascriptExecutor jseclickEdit = (JavascriptExecutor)driver;
		jseclickEdit.executeScript("arguments[0].click();", clickEdit); 
		
		WebElement dueDate = driver.findElement(By.xpath("//label[text()='Due Date']//following::button[@title='Select a date for Due Date']"));
		JavascriptExecutor jsedueDate = (JavascriptExecutor)driver;
		jsedueDate.executeScript("arguments[0].click();", dueDate);
		
		WebElement nextMonth = driver.findElement(By.xpath("//button[@title='Next Month']"));
		JavascriptExecutor jsenextMonth = (JavascriptExecutor)driver;
		jsenextMonth.executeScript("arguments[0].click();", nextMonth);
		
		WebElement selectDate = driver.findElement(By.xpath("//table[@class='slds-datepicker__month']//tr[3]/td[6]/span"));
		JavascriptExecutor jseselectDate = (JavascriptExecutor)driver;
		jseselectDate.executeScript("arguments[0].click();", selectDate);
		
		driver.findElement(By.xpath("//img[@title='Accounts']")).click();
		WebElement relatedToName = driver.findElement(By.xpath("//span[@title='Accounts']"));
		JavascriptExecutor jserelatedToName = (JavascriptExecutor)driver;
		jserelatedToName.executeScript("arguments[0].click();", relatedToName);
		
		driver.findElement(By.xpath("//input[@placeholder='Search Accounts...']")).sendKeys("test");
		WebElement selectAccounts = driver.findElement(By.xpath("//div[@title='TestAccount01']"));
		JavascriptExecutor jseselectAccounts = (JavascriptExecutor)driver;
		jseselectAccounts.executeScript("arguments[0].click();", selectAccounts);
		
		
		WebElement saveButton = driver.findElement(By.xpath("(//span[text()='Save'])[2]"));
		JavascriptExecutor jseclickSave = (JavascriptExecutor)driver;
		jseclickSave.executeScript("arguments[0].click();", saveButton);

		String text1 = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
		System.out.println(text1);
		
	}

}
