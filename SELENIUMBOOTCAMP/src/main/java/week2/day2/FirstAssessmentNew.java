package week2.day2;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FirstAssessmentNew {
	
	public static void main(String[] args) throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver (options);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		//1.Login to https://login.salesforce.com - https://login.salesforce.com/
		//username: makaia@testleaf.com
		//password: BootcampSel$123
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(10000);
		
//		 2. Click on the toggle menu button from the left corner
			driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
			Thread.sleep(5000);
			
			//	 3. Click View All and click Service Console from App Launcher
			driver.findElement(By.xpath("//p[text()='Service Console']")).click();
			Thread.sleep(5000);
			
			//	 4. Click on the Dropdown icon in the Individuals tab
			driver.findElement(By.xpath("//button[@title='Show Navigation Menu']")).click();
			
			//Select Home from the DropDown
			driver.findElement(By.xpath("//span[text()='Home']")).click();
			Thread.sleep(2000);
			
			//Select Dashboards from DropDown
			driver.findElement(By.xpath("//button[@title='Show Navigation Menu']")).click();
			driver.findElement(By.xpath("//a[@class='menuItem']//span[text()='Dashboards']")).click();
			
			//Click on New Dashboard
			driver.findElement(By.xpath("//div[text()='New Dashboard']")).click();
			WebElement frameOne = driver.findElement(By.xpath("//iframe[@title='dashboard']"));
			Thread.sleep(3000);
			driver.switchTo().frame(frameOne);
			Thread.sleep(9000);
			
			//Enter the Dashboard name as "YourName_Workout"
			String workOutName = "KavithaTA_Workout1";
			driver.findElement(By.id("dashboardNameInput")).sendKeys(workOutName);
			
			//Enter Description as Testing and Click on Create
			driver.findElement(By.id("dashboardDescriptionInput")).sendKeys("Testing");
			driver.findElement(By.id("submitBtn")).click();
			driver.switchTo().defaultContent();
			Thread.sleep(7000);
						
			WebElement frameTwo = driver.findElement(By.xpath("//iframe[@title='dashboard']"));
			// WebElement tableView = driver.findElement(By.xpath("//span[text()='Display as Table']"));
			//await driver.switchTo().frame(frameTwo);
		        //JavascriptExecutor jseFrameTwo = (JavascriptExecutor)driver;
		        //jseFrameTwo.executeScript("arguments[0].click();", frameTwo); 
			driver.switchTo().frame(frameTwo);
			Thread.sleep(3000);
			
			//Click on Done
			driver.findElement(By.xpath("(//button[text()='Done'])")).click();
			
			//driver.switchTo().defaultContent();
			String text = driver.findElement(By.xpath("//span[text()='Dashboard']/following-sibling::span[@class='slds-page-header__title slds-truncate']")).getText();
			System.out.println("Dashboard is Created : " +text);
			driver.findElement(By.xpath("//button[text()='Subscribe']")).click();
			driver.switchTo().defaultContent();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//span[@class='slds-radio--faux button-group-button-label' and text()='Daily']")).click();
			Select time = new Select(driver.findElement(By.id("time")));
			time.selectByVisibleText("10:00 AM");
			Thread.sleep(3000);
			driver.findElement(By.xpath("//span[text()='Save']")).click();
			Thread.sleep(3000);
			String text3 = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
			System.out.println(text3);
			if(text3.equals("You started a dashboard subscription."))
				System.out.println("\"You started Dashboard Subscription\" message is displayed");
			else 
				System.out.println("No message displayed");
			
			//Close the "YourName_Workout" tab
			WebElement findElement = driver.findElement(By.xpath("//span[@class='slds-assistive-text' and contains(text(),'Actions for')]"));
			
			//WebElement stage = driver.findElement(By.xpath("//button[contains(@class,'slds-combobox__input')]"));
	        JavascriptExecutor jse = (JavascriptExecutor)driver;
	        jse.executeScript("arguments[0].click();", findElement); 
			WebElement closeTab = driver.findElement(By.xpath("//span[text()='Close Tab']"));
			JavascriptExecutor jse1 = (JavascriptExecutor)driver;
			jse1.executeScript("arguments[0].click();", closeTab); 
			System.out.println("Tab is closed");
			
			//Click on Private Dashboards
			driver.findElement(By.xpath("//button[@title='Show Navigation Menu']")).click();
			Thread.sleep(2000);
			WebElement clickDashboard =driver.findElement(By.xpath("//a[@class='menuItem']//span[text()='Dashboards']"));
			
			//WebElement clickdashboard = driver.findElement(By.xpath("//span[text()='Close Tab']"));
			JavascriptExecutor jse2 = (JavascriptExecutor)driver;
			jse2.executeScript("arguments[0].click();", clickDashboard); 
			Thread.sleep(5000);
			driver.findElement(By.linkText("Private Dashboards")).click();
			
			//Verify the newly created Dashboard available
			driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).sendKeys(workOutName + Keys.ENTER);
			Thread.sleep(5000);                                                                                                
			//driver.findElement(By.xpath("//table[contains(@class,'slds-table slds-table_header-fixed')]//tr[1]//td[6]")).click();
			
			List<WebElement> rows = driver.findElements(By.xpath("//table[contains(@class,'slds-table slds-table_header-fixed')]//tr"));
			int rowsCount = rows.size();
			System.out.println("No. Of Rows:" + rowsCount);
			for(int i=1;i<=rowsCount;i++)
			{
				driver.findElement(By.xpath("//table[contains(@class,'slds-table slds-table_header-fixed')]//tr[i]//td[6]")).click();
				WebElement deleteDashboard = driver.findElement(By.xpath("//span[text()='Delete']"));
				JavascriptExecutor jse3 = (JavascriptExecutor)driver;
				jse3.executeScript("arguments[0].click();", deleteDashboard);
				Thread.sleep(5000);
				driver.findElement(By.xpath("//button[contains(@class,'slds-button slds-button--neutral uiButton--default')] /span[text()='Delete']")).click();
			}
			String deleteworkout = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
			System.out.println(deleteworkout);
	}

}
