package week1.day2;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateIndividuals extends BaseClass{
@Test
	public  void createIndividuals() throws InterruptedException {
	
		//	 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		//	 3. Click View All and click Individuals from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		//	 4. Click on the Dropdown icon in the Individuals tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Individuals");
		driver.findElement(By.xpath("//mark[text()='Individuals']")).click();
		//	 5. Click on New Individual
		driver.findElement(By.xpath("//div[contains(@class,'slds-col slds-no-flex')]//div[text()='New']")).click();
		Thread.sleep(5000);
		//	 6. Enter the Last Name as 'Kumar'
		String lastName = "Kavitha T A";
		driver.findElement(By.xpath("//input[@placeholder='Last Name']")).sendKeys(lastName);
		//	 7.Click save and verify Individuals Name
		driver.findElement(By.xpath("//button[@title='Save']/span[text()='Save']")).click();
		Thread.sleep(5000);
		String text = driver.findElement(By.xpath("//div[@class='entityNameTitle slds-line-height_reset']//following::span[@class='uiOutputText']")).getText();
		System.out.println("Name of the Individual Created :" + text);
		if (text.equals(lastName))
			System.out.println("Individual Created Successfully");
		else
		System.out.println("Individual is not Created");
			
			
	}

}
