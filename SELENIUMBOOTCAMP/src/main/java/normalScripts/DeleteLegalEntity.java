package normalScripts;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteLegalEntity {

		public static void main(String[] args) throws InterruptedException {
		
			WebDriverManager.chromedriver().setup();
			
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			ChromeDriver driver = new ChromeDriver (options);
			//Login to https://login.salesforce.com - https://login.salesforce.com/
			//username: makaia@testleaf.com
			//password: BootcampSel$123
			driver.get("https://login.salesforce.com");
			driver.manage().window().maximize();
			driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
			driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
			driver.findElement(By.id("Login")).click();
			Thread.sleep(7000);
			//	2. Click on the toggle menu button from the left corner
			driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
			Thread.sleep(5000);
			//	3. Click View All and click Legal Entities from App Launcher
			driver.findElement(By.xpath("//button[text()= 'View All']")).click();
			Thread.sleep(5000);
			driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Legal Entities");
			driver.findElement(By.xpath("//mark[text()='Legal Entities']")).click();
			Thread.sleep(5000);
			//	4. Click on the Dropdown icon in the legal Entities tab
			//	5. Click on New Legal Entity
			driver.findElement(By.xpath("//div[text()='New']")).click();
			Thread.sleep(5000);
			//	6. Enter Name as 'Salesforce Automation by Your Name'
			String text = "Test Salesforce Automation by Kavitha T A";
			driver.findElement(By.xpath("//div[contains(@class,'uiInput uiInputText')]//input")).sendKeys(text);
			
			//	7.Click save and verify Legal Entity Name
			driver.findElement(By.xpath("//button[@title='Save']/span[text()='Save']")).click();
			Thread.sleep(5000);
			String text1 = driver.findElement(By.xpath("(//div[contains(@class,'slds-form-element__control')]/span)[1]")).getText();
			System.out.println(text1);
			
	        if (text1.equals(text))
	        System.out.println("The Legal Entity is created Successfully");
	        else
	        	System.out.println("The Legal Entity is not created");
			//	6. Click on the Dropdown icon and Select Delete
	        driver.findElement(By.xpath("//div[@class='uiPopupTrigger']//a[contains(@class,'slds-grid slds-grid--vertical-align-center')]")).click();
	        driver.findElement(By.xpath("//a[@title='Delete']")).click();
		
		//	7.Click on the Delete option in the displayed popup window.
	        driver.findElement(By.xpath("//button[@class='slds-button slds-button--neutral uiButton--default uiButton--brand uiButton forceActionButton']/span")).click();
	        System.out.println("Legal Entity is deleted Successfully");
		//	8. Verify Whether Legal Entity is Deleted using Legal Entity Name
	        Thread.sleep(5000);
	        driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys(text + Keys.ENTER);
	        String text2 = driver.findElement(By.xpath("//span[text()='No items to display.']")).getText();
	        if (text2.equals( "No items to display"))
	        	System.out.println("Legal Entity is deleted");
	        else
	        	System.out.println("Legal Entity is not deleted");
	}

}
