package normalScripts;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateAccount extends BaseClass{

	public static void main(String[] args) throws InterruptedException {	
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("-disable-notifications");
		WebDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		//Thread.sleep(5000);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		//Login to https://login.salesforce.com
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(5000);
		//	 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		//	 3. Click View All and click Opportunities from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		//	 4. Click on the Dropdown icon in the Opportunities tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Accounts");
		driver.findElement(By.xpath("//mark[text()='Accounts']")).click();
//		 5. Click on New Individual
			driver.findElement(By.xpath("//a[@title='New']/div")).click();
			Thread.sleep(5000);
		driver.findElement(By.xpath("//label[text()='Account Name']/following::input[@name='Name']")).sendKeys("Kavitha T A");
		
		WebElement clickOwnership = driver.findElement(By.xpath("//label[text()='Ownership']/following::button[@aria-label='Ownership, --None--']"));
		JavascriptExecutor jseclickOwnership = (JavascriptExecutor)driver;
		jseclickOwnership.executeScript("arguments[0].click();", clickOwnership);
	/*
	 * WebDriverWait clickOwnershipwait = new WebDriverWait(driver,30); WebElement
	 * clickOwnership = driver.findElement(By.
	 * xpath("//label[text()='Ownership']/following::button[@aria-label='Ownership, --None--']"
	 * ));
	 * clickOwnershipwait.until(ExpectedConditions.visibilityOf(clickOwnership));
	 * clickOwnership.click();
	 */
		WebElement selectOwnership = driver.findElement(By.xpath("//span[@title='Public']"));
		JavascriptExecutor jseselectOwnership = (JavascriptExecutor)driver;
		jseselectOwnership.executeScript("arguments[0].click();", selectOwnership);
		
		
		driver.findElement(By.xpath("//button[text()='Save']")).click();
		Thread.sleep(5000);
		String accountStatus = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
		System.out.println("AccountStatus :" + accountStatus);
		
		driver.close();
		
	}

}
