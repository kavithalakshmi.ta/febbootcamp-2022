package normalScripts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class LearnDatePicker {

	public static void main(String[] args) throws ParseException {
		
		Calendar calendar = Calendar.getInstance();
		//System.out.println(calendar);
		String targetDate = "28-Jan-2022";
		SimpleDateFormat targetDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date formattedTargetDate;
		try {
			targetDateFormat.setLenient(false);
			formattedTargetDate = targetDateFormat.parse(targetDate);
			System.out.println(formattedTargetDate);
		}
		catch (ParseException e) {
			new Exception("Invalid date is provided, please check the input date");
		}
		
	}
}
