package week1.day2;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateNewTask extends BaseClass{
@Test
	public void createNewTask() throws InterruptedException {
	
		//	 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		//	 3. Click View All and click Sales from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		//	 4. Click on the Dropdown icon in the Opportunities tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Content");
		driver.findElement(By.xpath("//mark[text()='Content']")).click();
		//5. Click View All from Today's Task
		driver.findElement(By.xpath("(//a[@class='viewAllLink']/span[text()='View All'])[2]")).click();
		Thread.sleep(3000);
		//6. Click on Show one more Action and click New Task
		WebElement splitViewDropDown = driver.findElement(By.xpath("//span[text()='Display as Split View']"));
		JavascriptExecutor jsesplitview = (JavascriptExecutor)driver;
		jsesplitview.executeScript("arguments[0].click();", splitViewDropDown);
		
		WebElement tableView = driver.findElement(By.xpath("//span[text()='Table']"));
		JavascriptExecutor jseTabView = (JavascriptExecutor)driver;
		jseTabView.executeScript("arguments[0].click();", tableView); 
		
		driver.findElement(By.xpath("//div[text()='New Task']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@placeholder='Search People...']")).clear();
		driver.findElement(By.xpath("//input[@placeholder='Search People...']")).sendKeys("Derrick");
		Thread.sleep(3000);
		WebElement pickList = driver.findElement(By.xpath("//div[@class='primaryLabel slds-truncate slds-lookup__result-text']"));
		JavascriptExecutor jsepickList = (JavascriptExecutor)driver;
		jsepickList.executeScript("arguments[0].click();", pickList); 
		
		driver.findElement(By.xpath("//a[text()='Not Started']")).click();
		WebElement selectStatus = driver.findElement(By.xpath("//a[@title='In Progress']"));
		JavascriptExecutor jseselectStatus = (JavascriptExecutor)driver;
		jseselectStatus.executeScript("arguments[0].click();", selectStatus); 
		
		driver.findElement(By.xpath("//input[@class='slds-combobox__input slds-input']")).click();
		Thread.sleep(3000);
		WebElement subject = driver.findElement(By.xpath("//span[@title='Email']"));
		JavascriptExecutor jsesubject = (JavascriptExecutor)driver;
		jsesubject.executeScript("arguments[0].click();", subject); 
		
		driver.findElement(By.xpath("//a[text()='Normal']")).click();
		WebElement priority = driver.findElement(By.xpath("//a[@title='High']"));
		JavascriptExecutor jsepriority = (JavascriptExecutor)driver;
		jsepriority.executeScript("arguments[0].click();", priority);
		
		driver.findElement(By.xpath("//img[@title='Contacts']")).click();
		WebElement contactName = driver.findElement(By.xpath("//a[@title='Contacts']"));
		JavascriptExecutor jsecontactName = (JavascriptExecutor)driver;
		jsecontactName.executeScript("arguments[0].click();", contactName);
		
		driver.findElement(By.xpath("//input[@placeholder='Search Contacts...']")).sendKeys("Kavitha");
		//System.out.println(text);
		Thread.sleep(3000);
		/* WebElement newContact = driver.findElement(By.xpath("//span[@title='New Contact']"));
		JavascriptExecutor jseContact = (JavascriptExecutor)driver;
		jseContact.executeScript("arguments[0].click();", newContact);
		
		driver.findElement(By.xpath("(//a[@class='select'])[3]")).click();
		WebElement newContactName = driver.findElement(By.xpath("//a[@title='Ms.']"));
		JavascriptExecutor jseNewContact = (JavascriptExecutor)driver;
		jseNewContact.executeScript("arguments[0].click();", newContactName);
		
		//driver.findElement(By.xpath("//input[@placeholder='First Name']")).sendKeys("Kavitha");
		//driver.findElement(By.xpath("//input[@placeholder='Last Name']")).sendKeys("Angappan");
		//driver.findElement(By.xpath("//input[@inputmode='email']")).sendKeys("test@gmail.com");
		//Thread.sleep(3000);
		//WebElement saveButton = driver.findElement(By.xpath("(//span[text()='Save'])[2]"));
		//JavascriptExecutor jseclickSave = (JavascriptExecutor)driver;
		//jseclickSave.executeScript("arguments[0].click();", saveButton);
		
		String text1 = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
		System.out.println(text1);*/
		
		driver.findElement(By.xpath("//div[@title='Kavitha lakshmi']")).click();
		
		driver.findElement(By.xpath("//img[@title='Accounts']")).click();
		WebElement relatedToName = driver.findElement(By.xpath("//span[@title='Products']"));
		JavascriptExecutor jserelatedToName = (JavascriptExecutor)driver;
		jserelatedToName.executeScript("arguments[0].click();", relatedToName);
		
		
		WebElement saveButton = driver.findElement(By.xpath("(//span[text()='Save'])[2]"));
				JavascriptExecutor jseclickSave = (JavascriptExecutor)driver;
				jseclickSave.executeScript("arguments[0].click();", saveButton);
		
				String text1 = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
				System.out.println(text1);
		
	}

}
