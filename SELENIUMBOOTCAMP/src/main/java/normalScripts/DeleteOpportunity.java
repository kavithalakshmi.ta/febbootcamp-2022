package normalScripts;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.it.Date;
import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteOpportunity {
//@Test
	// public void createOpportunity() throws InterruptedException {
	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("-disable-notifications");
		WebDriver driver = new ChromeDriver(options);
		// driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		// Thread.sleep(5000);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		// Login to https://login.salesforce.com
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(5000);
		String title = driver.getTitle();
		System.out.println("Title :" + title);

		WebDriverWait toggleButtonwait = new WebDriverWait(driver, 30);
		WebElement toggleButton = driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"));
		toggleButtonwait.until(ExpectedConditions.visibilityOf(toggleButton));

		// 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		// 3. Click View All and click Opportunities from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		// 4. Click on the Dropdown icon in the Opportunities tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Opportunities");
		driver.findElement(By.xpath("//mark[text()='Opportunities']")).click();

		WebDriverWait searchListwait = new WebDriverWait(driver, 30);
		WebElement searchList = driver.findElement(By.xpath("//input[@placeholder='Search this list...']"));
		searchListwait.until(ExpectedConditions.visibilityOf(searchList));

//		 5. Click on New Individual
		driver.findElement(By.xpath("//input[@placeholder='Search this list...']"))
				.sendKeys("Sales Force Automation By Kavitha T A");
		Thread.sleep(5000);
		// 6. Enter the Last Date as 'Kumar'
		// String lastName = "Kavitha T A";
		WebElement showActions = driver.findElement(
				By.xpath("//table[@aria-label='Recently Viewed']/tbody/tr[1]/td//span[text()='Show Actions']"));

		JavascriptExecutor jseshowActions = (JavascriptExecutor) driver;
		jseshowActions.executeScript("arguments[0].click();", showActions);

		WebElement selectDelete = driver.findElement(By.xpath("//a[@title='Delete']"));
		JavascriptExecutor jseselectDelete = (JavascriptExecutor) driver;
		jseselectDelete.executeScript("arguments[0].click();", selectDelete);

		driver.findElement(By.xpath("//span[text()='Delete']")).click();

		
		 String deleteMessage = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
		 System.out.println("DeleteMessage : "+deleteMessage);
		if (deleteMessage.equals("Opportunity \"Sales Force Automation By Kavitha T A\" was deleted. Undo"))
				System.out.println("Oppurtunity is Successfully deleted");
		else
			System.out.println("Oppurtunity is Not deleted");

	 }

}
