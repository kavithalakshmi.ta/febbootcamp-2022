package normalScripts;

import java.time.Duration;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateOpportunitywithoutMandatoryFields {
//@Test
	//public void createOpportunity() throws InterruptedException {
	public static void main(String[] args) throws InterruptedException {	
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("-disable-notifications");
		WebDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		//Thread.sleep(5000);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		//Login to https://login.salesforce.com
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(5000);
		//	 2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(5000);
		//	 3. Click View All and click Opportunities from App Launcher
		driver.findElement(By.xpath("//button[text()= 'View All']")).click();
		Thread.sleep(5000);
		//	 4. Click on the Dropdown icon in the Opportunities tab
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Opportunities");
		driver.findElement(By.xpath("//mark[text()='Opportunities']")).click();
//		 5. Click on New Individual
			driver.findElement(By.xpath("//a[@title='New']/div")).click();
			Thread.sleep(5000);
			//	 6. Enter the Last Date as 'Kumar'
			//String lastName = "Kavitha T A";
			driver.findElement(By.xpath("//input[@name='CloseDate']")).click();
			driver.findElement(By.xpath("(//table[@class='slds-datepicker__month']/tbody/tr/td)[5]")).click();
			//	 7.Click save and verify Opportunity Name
			/*
			 * driver.findElement(By.
			 * xpath("//label[text()='Opportunity Name']//following::input[@name='Name']")).
			 * sendKeys("Sales Force Automation By Kavitha T A");
			 * 
			 * WebElement stage = driver.findElement(By.xpath(
			 * "//button[contains(@class,'slds-combobox__input')]")); JavascriptExecutor jse
			 * = (JavascriptExecutor)driver; jse.executeScript("arguments[0].click();",
			 * stage);
			 * 
			 * WebElement stageOption =
			 * driver.findElement(By.xpath("//span[@title='Needs Analysis']"));
			 * JavascriptExecutor executor3 = (JavascriptExecutor)driver;
			 * executor3.executeScript("arguments[0].click();", stageOption);
			 */

			Thread.sleep(5000);
		//	driver.findElement(By.xpath("//button[contains(@class,'slds-combobox__input')]/span[text()='Needs Analysis']")).click();
			driver.findElement(By.xpath("//button[@name='SaveEdit']")).click();
			//Verify the Alert message (Complete this field) displayed for Name and Stage
			Thread.sleep(5000);
			
			
			//driver.findElement(By.id("window")).isDisplayed();
			//System.out.println("Error Button is displayed");
			 //driver.switchTo().alert();
			String alertText = driver.findElement(By.xpath("//div[@class='fieldLevelErrors']")).getText();
			System.out.println(alertText);
			
			
			WebElement alertClose = driver.findElement(By.xpath("//span[text()='Close error dialog']"));
			JavascriptExecutor jsealertClose = (JavascriptExecutor)driver;
			jsealertClose.executeScript("arguments[0].click();", alertClose);
			
			String errorText = driver.findElement(By.xpath("//div[text()='Complete this field.']")).getText();
			System.out.println("ErrorText :" +errorText );
			
			if (errorText.equals("Complete this field."))
				System.out.println("Error Message is Displayed");
			else
				System.out.println("Error Message is Not Displayed");
			
			driver.close();
	}

}
