package TestNGScripts;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	public static ChromeDriver driver;
	//@Parameters({"userName" , "password", "url"})
	@BeforeMethod
	public void preCondition() throws InterruptedException {
		//public static void main(String[] args) throws InterruptedException, IOException {	
			//To setup the file path to read the data
			//FileInputStream fis = new FileInputStream("./TestData/NewConfig.properties");
			
			//create object for properties
			//Properties prop = new Properties();
			
			//Load the properties files
			//prop.load(fis);	
			
			
			
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("-disable-notifications");
		 driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5000));
		//Thread.sleep(5000);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		//Login to https://login.salesforce.com
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(5000);
		
	}
@AfterMethod	
public void postCondition() {
	driver.close();
	}
	}